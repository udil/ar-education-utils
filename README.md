**Dependencies**
- macOS
- coreutils
- ffmpeg

install dependencies with:
- `brew install coreutils`
- `brew install ffmpeg`

**Usage**

`main.sh <input_dir> <output_file> <temp_dir>`

example: `/path/to/main.sh /path/to/video_pairs_directory /path/to/output.mp4 /path/to/temp_dir`

**Arguments**

input: `path/to/video_pairs_directory` has a list of files 01_something_text.mp4 (required), 01_something_man.mp4 (optional), 01_something_voiceover.aifc (optional), 01_something_sfx.wav (optional), and so on 02_something_text.mp4... The suffixes (_text.mp4, _man.mp4, _voiceover.aifc, _sfx.wav) are crucial for now, and the prefixes (01_something) must all match for a given video section.

output: `/path/to/output.mp4` will be a composite of the man+text.mp4 videos and audio files merged together, and then concatenated together, each with their alphanumeric order in the directory number overlaid in the top left of the video to indicate where in the sequence it is

temp: `/path/to/temp_dir` is a temporary folder that WILL be deleted at the end of this. You can preview the joined/merged files here as they get created
