#!/bin/bash

ECHO_PREFIX="AR WALL --- "

INPUT_DIR=$(realpath "$1")
OUTPUT_PATH=$(realpath "$2")
RM_INPUT_DIR=${3:-false}
files=($( ls "$INPUT_DIR" ))

content=""
for ((i=0; i<${#files[@]}; i++)); do
  files[$i]="$INPUT_DIR/${files[$i]}"
  content+="file '${files[$i]}'"
  if ! [ "$i" = "$(( ${#files[@]} - 1))" ] ; then
    content+="\n"
  fi
done

FILE_NAMES_DOC="$INPUT_DIR/../tmp.txt"
FILE_NAMES_DOC=$(realpath "$FILE_NAMES_DOC")
echo "$ECHO_PREFIX creating temp file with joined file name listings at:$FILE_NAMES_DOC"
echo -e "$content" > "$FILE_NAMES_DOC"
echo "$ECHO_PREFIX concatenating videos to $OUTPUT_PATH..."
echo "$ECHO_PREFIX This may take a moment without progress text. There are ${#files[@]} videos to concatenate"
ffmpeg -hide_banner -loglevel warning -f concat -safe 0 -protocol_whitelist "file,http,https,tcp,tls" -i "$FILE_NAMES_DOC" -vcodec libx264 -crf 28 "$OUTPUT_PATH"
if [ "$RM_INPUT_DIR" = "true" ] ; then
  echo "$ECHO_PREFIX removing temp files and folders"
  rm -r "$INPUT_DIR"
  rm -r "$FILE_NAMES_DOC"
else
  echo "$ECHO_PREFIX keeping temp files and folders at:"
  echo "$ECHO_PREFIX $INPUT_DIR"
  echo "$ECHO_PREFIX $FILE_NAMES_DOC"
fi
echo "$ECHO_PREFIX Done!"
echo "$ECHO_PREFIX $OUTPUT_PATH"
