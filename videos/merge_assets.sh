#!/bin/bash

getInput() {
  local input="$1"
  local output=""
  if [ -n "$input" ] ; then
    output=" -i $input"
  fi
  echo "$output"
}

if [ -n "$1" ] ; then
  V1=$(realpath "$1")
fi
if [ -n "$2" ] ; then
  V2=$(realpath "$2")
fi

V2_MASK=$(realpath "$3")

if [ -n "$4" ] ; then
  A1=$(realpath "$4")
fi
if [ -n "$5" ] ; then
  A2=$(realpath "$5")
fi

OVERLAY_TEXT="$6"
OUTPUT_PATH=$(realpath "$7")
ECHO_PREFIX="AR WALL --- "

OUTPUT_DIR="$( dirname "$OUTPUT_PATH" )"

TMP1="$OUTPUT_DIR/tmp-${OVERLAY_TEXT}-1.mp4"
if [ -f "$V2" ] ; then
  V1_RES=$(ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of csv=s=x:p=0 "$V1")
  V2_RES=$(ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of csv=s=x:p=0 "$V2")

  echo "$ECHO_PREFIX applying video merge"
  FILTER="[0:v]setpts=PTS-STARTPTS, scale=${V1_RES}[top]; \
          [1:v]setpts=PTS-STARTPTS, scale=${V2_RES}[bottom]; \
          [2:v]alphaextract[mask]; \
          [bottom][mask]alphamerge[maskedBottom]; \
          [top][maskedBottom]overlay=x=(W-w)/2 + 40: y=(H-h)/2 + 200: eof_action=pass"
  ffmpeg -hide_banner -loglevel warning -i "$V1" -i "$V2" -loop 1 -i "$V2_MASK" -filter_complex "$FILTER" "$TMP1"
else
  echo "$ECHO_PREFIX no second video. Using the first video as input for upcoming audio/textoverlay merges"
  cp "$V1" "$TMP1"
fi

AUDIO_INPUTS=$(getInput "$A1")
AUDIO_INPUTS+=$(getInput "$A2")
TMP2="$OUTPUT_DIR/tmp-${OVERLAY_TEXT}-2.mp4"
TMP3="$OUTPUT_DIR/tmp-${OVERLAY_TEXT}-3.mp4"

echo "$ECHO_PREFIX adding silence for empty audio inputs or audio inputs shorter than video"
ffmpeg -hide_banner -loglevel warning -i "$TMP1" -f lavfi -i anullsrc=channel_layout=stereo:sample_rate=48000 -c:v copy -c:a aac -shortest "$TMP2"

if [ -z "$AUDIO_INPUTS" ] ; then
  TMP3="$TMP2"
elif [ -z "$A1" ] || [ -z "$A2" ] ; then #only one empty, add single audio
  echo "$ECHO_PREFIX adding single audio source"
  ffmpeg -hide_banner -loglevel warning -i "$TMP2" $AUDIO_INPUTS -c:v copy -c:a aac -map 0:v -map 1:a -shortest "$TMP3"
else #both present
  echo "$ECHO_PREFIX mixing audio from two inputs"
  ffmpeg -hide_banner -loglevel warning -i "$TMP2" $AUDIO_INPUTS -filter_complex "[1][2]amix=inputs=2[a]" -map 0:v -map "[a]" -c:v copy -shortest "$TMP3"
fi

# add overlay text
echo "$ECHO_PREFIX adding overlay text"
ffmpeg -hide_banner -loglevel warning -i "$TMP3" -vf drawtext="text=$OVERLAY_TEXT: fontcolor=white: fontsize=50: box=1: boxcolor=black@0.5: boxborderw=5: x=20: y=20" "$OUTPUT_PATH"

function removeIfFile() {
  if [ -f "$1" ] ; then
    rm "$1"
  fi
}
removeIfFile "$TMP1"
removeIfFile "$TMP2"
removeIfFile "$TMP3"