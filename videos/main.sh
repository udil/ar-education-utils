#!/bin/bash

INPUT_DIR="$(realpath "$1")"
OUTPUT_PATH="$(realpath "$2")"
TEMP_DIR="${3:-$INPUT_DIR/../tmp}"
TEMP_DIR="$(realpath "$TEMP_DIR")"

MASK_PATH="$(dirname "${BASH_SOURCE[0]}" )/../pictures/mannequin-alpha.png"
MASK_PATH="$(realpath "$MASK_PATH")"

ECHO_PREFIX="AR WALL --- "
echo "$ECHO_PREFIX making temp directory for merged videos at $TEMP_DIR"
mkdir -p "$TEMP_DIR"

echo "$ECHO_PREFIX merging videos..."
./merge_all_assets.sh "$INPUT_DIR" "$MASK_PATH" "$TEMP_DIR"
./concat_videos.sh "$TEMP_DIR" "$OUTPUT_PATH" "true"