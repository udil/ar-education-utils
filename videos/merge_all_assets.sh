#!/bin/bash

src_dir=$(realpath "$1")
mask=$(realpath "$2")
targ_dir=$(realpath "$3")

ECHO_PREFIX="AR WALL --- "
fileNames=($( ls "$src_dir" ))
newLineFilePaths=""

for ((i=0; i<${#fileNames[@]}; i++)); do
  newName=${fileNames[$i]%_man.mp4}
  newName=${newName%_text.mp4}
  newName=${newName%_sfx.wav}
  newName=${newName%_voiceover.aifc}
  newLineFilePaths+="$src_dir/$newName"
  if ! [ "$i" = "$(( ${#fileNames[@]} - 1))" ] ; then
    newLineFilePaths+="\n"
  fi
done
uniqueFilePaths=$(echo -e "$newLineFilePaths" | sort | uniq)
echo "$ECHO_PREFIX the following unique file names were found"
echo "$uniqueFilePaths"

uniqueFilePathArray=($uniqueFilePaths)
echo "$ECHO_PREFIX ${#uniqueFilePathArray[@]} video pairs to merge"
for ((i=0; i<${#uniqueFilePathArray[@]}; i++)); do
  uniqueFilePath=${uniqueFilePathArray[$i]}
  uniqueFileName=$(basename "$uniqueFilePath")
  #prefix is like 001000_, so first six chars are the unique sorting identifiers
  fileNameNumericPrefix=${uniqueFileName:0:6}
  man="${uniqueFilePath}_man.mp4"
  text="${uniqueFilePath}_text.mp4"
  sfx="${uniqueFilePath}_sfx.wav"
  voiceover="${uniqueFilePath}_voiceover.aifc"
  if ! [ -f "$sfx" ] ; then
    sfx=""
    echo "$ECHO_PREFIX no sfx file found for $uniqueFilePath"
  fi
  if ! [ -f "$voiceover" ] ; then
    voiceover=""
    echo "$ECHO_PREFIX no voiceover file found for $uniqueFilePath"
  fi
  overlay_text="$((i + 1))-$fileNameNumericPrefix"
  echo "$overlay_text"
  output_path="${targ_dir}/$(basename "$uniqueFilePath")_joined.mp4"
  ./merge_assets.sh "$text" "$man" "$mask" "$sfx" "$voiceover" "$overlay_text" "$output_path"
  echo "$ECHO_PREFIX $((i + 1)) / ${#uniqueFilePathArray[@]} merged"
done